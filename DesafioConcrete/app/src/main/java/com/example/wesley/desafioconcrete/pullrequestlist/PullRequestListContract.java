package com.example.wesley.desafioconcrete.pullrequestlist;

import com.example.wesley.desafioconcrete.base.BasePresenter;
import com.example.wesley.desafioconcrete.base.BaseView;
import com.example.wesley.desafioconcrete.model.PullRequest;
import com.example.wesley.desafioconcrete.model.Repository;

import java.util.List;

/**
 * Created by wesley on 9/15/16.
 */
public interface PullRequestListContract {

    interface View extends BaseView<Presenter> {
        void pullRequestListReceived(List<PullRequest> requestList);
    }

    interface Presenter extends BasePresenter {
        void loadPullRequestList(Repository repo);
    }
}
