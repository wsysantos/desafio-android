package com.example.wesley.desafioconcrete.repositorylist;

import com.example.wesley.desafioconcrete.base.BasePresenter;
import com.example.wesley.desafioconcrete.base.BaseView;
import com.example.wesley.desafioconcrete.model.Repository;

import java.util.List;

/**
 * Created by wesley on 9/15/16.
 */
public interface RepositoryListContract {

    interface View extends BaseView<Presenter> {
        void repositoriesReceived(List<Repository> repositoryList);
    }

    interface Presenter extends BasePresenter {
        void loadRepositoriesList(int pageNumber);
    }
}
