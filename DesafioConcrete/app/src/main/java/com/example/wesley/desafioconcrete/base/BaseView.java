package com.example.wesley.desafioconcrete.base;

import com.example.wesley.desafioconcrete.model.User;

/**
 * Created by wesley on 9/15/16.
 */
public interface BaseView<T> {
    void setPresenter(T presenter);
    void showLoading();
    void hideLoading();
    void errorReceived(Throwable e);
}
