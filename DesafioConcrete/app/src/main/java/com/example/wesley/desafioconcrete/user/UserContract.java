package com.example.wesley.desafioconcrete.user;

import com.example.wesley.desafioconcrete.base.BasePresenter;
import com.example.wesley.desafioconcrete.base.BaseView;
import com.example.wesley.desafioconcrete.model.User;

/**
 * Created by wesley on 9/16/16.
 */
public interface UserContract {

    interface View extends BaseView<Presenter> {
        void userReceived(User user);
    }

    interface Presenter extends BasePresenter {
        void gerUserByLogin(String login);
    }
}
